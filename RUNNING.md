# Requirements

- docker-compose
- Java/Maven

## Building

Create the Java applications docker images:

```
./build.sh
```

Build the other components:

```
docker-compose build
```

## Running

To start the application:

```
docker-compose up
```

This should bring up everything, just navigate to http://localhost:8080/

## Seed with some data

There is a seed application that will create some users and orders:

```
cd seed
npm install
node index.js
```
