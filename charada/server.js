'use strict';

const axios = require('axios');
const Hapi = require('hapi');

const server = Hapi.server({
	port: process.env.PORT ? process.env.PORT  : 3001,
	host: process.env.HOST ? process.env.HOST : 'localhost'
});


// Configure the needed backend microservices
const clientUrl = process.env.CLIENT_MS_URL ? process.env.CLIENT_MS_URL : 'http://localhost:8081/v1/clients'
const orderUrl = process.env.ORDER_MS_URL ? process.env.ORDER_MS_URL : 'http://localhost:8082/v1/orders'

let clientCache = {}

/**
 * searchClients queries the Client MS for clients based on filter
 * 
 * This method will reach the /v1/clients/search/byAll endpoint to search for
 * clients based on email, name and phone.
 * 
 * @param {object} filter 
 * 
 * @returns the list of client IDs
 */
function searchClients(filter) {
	const params = filter;
	const searchEndpoint = '/search/byAll';
	console.log('client search params: ', params)
	return axios.get(clientUrl + searchEndpoint, {params}).then(response => {
		const ids = response.data._embedded.clients.map(client => {
			// Save client on cache
			clientCache[client.id] = Object.assign({}, client)
			return client.id
		});
		return ids;
	});
}


/**
 * searchOrder queries the Order MS for a list of orders based on the specified
 * filter and a list of client IDs.
 * 
 * @param {object} filter Defines the order search parameters. The following are
 *                        supported: start and end UTC date strings.
 * @param {string[]} clients a list of client IDs to include orders from
 */
function searchOrder(filter, clients) {
	console.log("order search params: ", filter.start, filter.end);
	const params = {
		start: filter.start,
		end: filter.end,
		clientId: clients.join(",")
	};

	const searchEndpoint = '/search/byAll';

	return axios.get(orderUrl + searchEndpoint, {params})
		.then(
			response => {
				response.data._embedded.orders.forEach(order => {
					order.client = clientCache[order.clientId]
				});
				console.log('orders found: ', response.data._embedded.orders.length);
				return response.data;
			},
			error => {
				console.error(error)
			}
		)
}


// ----------------------------------------------------------------------------
// search endpoint
//
// Returns the list of orders given the filter.
// ----------------------------------------------------------------------------
server.route({
	method: 'GET',
	path: '/search',
	handler: (request, h) => {
		let filter = Object.assign({}, JSON.parse(request.query.clientFilter));

		if (!filter.name) filter.name = '';
		if (!filter.phone) filter.phone = '';
		if (!filter.email) filter.email = '';

		return searchClients(filter).then(ids => {
			console.log("clients found: ", ids.length);
			if (ids.length == 0) {
				return {'_embedded': {'orders': []}};
			}
			return searchOrder(request.query, ids);
		});
	}
});


// Hapi server start code 
const init = async () => {
	await server.start();
	console.log(`Server running at: ${server.info.uri}`);
};


// Keep node from crashing when some unhandled error is trhown by user code.
process.on('unhandledRejection', (err) => {
	console.error("--------------------------------------------------------------")
	console.error(err);
	console.error("--------------------------------------------------------------")
});

init();
