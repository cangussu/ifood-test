package com.ifood.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;


import com.ifood.demo.client.ClientEventHandler;
import com.ifood.demo.client.Client;

@SpringBootApplication
@EnableJpaRepositories("com.ifood.demo")
public class ClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(ClientApplication.class, args);
	}

	@Bean
	ClientEventHandler clientEventHandler() {
		return new ClientEventHandler();
	}

	@Configuration
	public class ExposeEntityIdRestConfiguration extends RepositoryRestConfigurerAdapter {
		@Override
		public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
			config.exposeIdsFor(Client.class);
		}
	}
}
