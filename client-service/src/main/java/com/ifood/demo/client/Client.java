package com.ifood.demo.client;

import java.util.UUID;

import javax.persistence.Id;

import org.springframework.data.mongodb.core.mapping.Document;


import lombok.Data;
import lombok.RequiredArgsConstructor;

@Document
@Data
@RequiredArgsConstructor
public class Client {

	private @Id UUID id = UUID.randomUUID();
	private final String name;
	private final String email;
	private final String phone;

	protected Client() {
		this(null, null, null);
	}
}
