import * as React from 'react';
import './App.css';

// import Grid from '@material-ui/core/Grid';
import {FilteredOrderList} from './containers/FilteredOrderList';
import {SearchContainer} from './containers/Search'

import Typography from '@material-ui/core/Typography';

import withRoot from './withRoot';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


class App extends React.Component {
  public render() {
    return (
      <div>
      <AppBar position="static" color="default">
        <Toolbar>
          <Typography variant="title" color="inherit">
            Order List
          </Typography>
        </Toolbar>
      </AppBar>
        <SearchContainer />
        <FilteredOrderList />
      </div>
    );
  }
}

export default withRoot(App);
