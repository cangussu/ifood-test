// src/components/Client.tsx

import * as React from 'react';

import Typography from '@material-ui/core/Typography';
import { createStyles, Theme } from '@material-ui/core';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';

// import Card from '@material-ui/core/Card';

import {IClient} from '../models'


// Theme-dependent styles
const styles = ({ palette, spacing }: Theme) => createStyles({
  card: {
    margin: '5px',
    padding: '5px',
    background: 'light-gray'
  },
  values: {
    fontWeight: 500
  },
  title: {
    // color: palette.primary.light
  }
});

interface IProps extends WithStyles<typeof styles> {
  client: IClient
}

class Client extends React.Component<IProps> {

  public render() {
    return (
      <div className={this.props.classes.card}>
        <Typography variant="subheading" className={this.props.classes.title}>
          Client Name: <span className={this.props.classes.values}>{this.props.client.name}</span>
        </Typography>

        <Typography variant="subheading">
          E-mail: <span className={this.props.classes.values}>{this.props.client.email}</span>
        </Typography>
        
        <Typography variant="subheading">
          Phone: <span className={this.props.classes.values}>{this.props.client.phone}</span>
        </Typography>
      </div>
      );
    }
}

export default withStyles(styles)(Client);