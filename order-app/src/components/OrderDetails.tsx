// src/components/OrderDetails.tsx

import * as React from 'react';

import {IItem, IOrder} from '../models'
import Client from './Client';

import './OrderDetails.css';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import { createStyles, Theme } from '@material-ui/core';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';

const styles = ({ palette, spacing }: Theme) => createStyles({
  head: {
    backgroundColor: '#90a4ae',
    color: palette.common.white,
  },
  body: {
    fontSize: 24,
  },
  root: {
    width: '100%',
    marginTop: spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    // minWidth: 300,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: palette.background.default,
    },
  },
});


interface IProps extends WithStyles<typeof styles> {
  order: IOrder
}

class OrderDetails extends React.Component<IProps> {

  public render() {  
    const items = this.props.order.items.map((item, idx) => {
      return (
        this.renderItem(item, idx)
      );
    });

    return (
      <div>
        <Client client={this.props.order.client} />
        <Paper className={this.props.classes.root}>
          <Table className={this.props.classes.table}>
            <TableHead className={this.props.classes.head}>
              <TableRow>
                <TableCell>Description</TableCell>
                <TableCell numeric={true}>Quantity</TableCell>
                <TableCell numeric={true}>Unit Price</TableCell>
                <TableCell numeric={true}>Total</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items}
            </TableBody>
          </Table>
        </Paper>
      </div>
    );
  }

  private renderItem(item: IItem, idx: number) {
    const total = (item.quantity * item.price).toFixed(2);
    return(
      <TableRow className={this.props.classes.row} key={idx}>
        <TableCell component="th" scope="row">{item.description}</TableCell>
        <TableCell numeric={true}>{item.quantity}</TableCell>
        <TableCell numeric={true}>{item.price}</TableCell>
        <TableCell numeric={true}>{total}</TableCell>
      </TableRow>
    );
  }

}

export default withStyles(styles)(OrderDetails);