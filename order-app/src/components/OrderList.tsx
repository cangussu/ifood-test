// src/components/OrderList.tsx

import * as React from 'react';

import {IOrder} from '../models';

import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';  
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import OrderDetails from './OrderDetails';

import { createStyles, Theme } from '@material-ui/core';
import withStyles, { WithStyles } from '@material-ui/core/styles/withStyles';


const styles = ({ palette, spacing }: Theme) => createStyles({
  head: {
    backgroundColor: '#b0bec5',
    color: palette.common.white,
    textAlign: 'right'
  },
  body: {
    fontSize: 14,
  },
  root: {
    width: '100%',
    marginTop: spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 300,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: palette.background.default,
    },
  },
});

interface IProps extends WithStyles<typeof styles> {
  orders: IOrder[];
}

interface IState {
  open: boolean;
  selectedOrder: IOrder;
}

class OrderList extends React.Component<IProps, IState> {
  
  constructor(props: IProps) {
    super(props);
    this.state = {
      open: false,
      selectedOrder: props.orders[0]
    };
  }

  public handleClose = () => {
    this.setState({open: false});
  }

  public cellClicked(order: any) {
    return (e : any) => {
      this.setState({
        open: true,
        selectedOrder: order
      });
    }
  }
  
  public render() {
    const items = this.props.orders.map((order, idx) => {
      const total = order.items.reduce((a, b) => a + (b.price * b.quantity), 0).toFixed(2);
      return (
        <TableRow onClick={this.cellClicked(order)} key={idx} className={this.props.classes.row}>
          <TableCell component="th" scope="row">{order.createdAt.toDateString()}</TableCell>
          <TableCell>{order.client.name}</TableCell>
          <TableCell>{order.client.phone}</TableCell>
          <TableCell>{order.client.email}</TableCell>
          <TableCell numeric={true}>{total}</TableCell>
        </TableRow>
      );
    });

    return (
      <Paper className={this.props.classes.root}>
        <Dialog open={this.state.open} onClose={this.handleClose} fullScreen={false}>
          <DialogTitle>Order Details</DialogTitle>
          <DialogContent>
            <DialogContent><OrderDetails order={this.state.selectedOrder} /></DialogContent>
          </DialogContent>
          <DialogActions>
            <Button color="primary" onClick={this.handleClose}>OK</Button>
          </DialogActions>
        </Dialog>
        <Table className={this.props.classes.table}>
          <TableHead className={this.props.classes.head}>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell>Client Name</TableCell>
              <TableCell>Phone</TableCell>
              <TableCell>E-Mail</TableCell>
              <TableCell numeric={true}>Total Value</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {items}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(OrderList);
