
# Requirements

Besides the Java dependencies, you will need:

- Node.js version v9.8.0
- Docker and docker-compose

# Running

## Start the microservices

After compiling, start with the same procedures described on README.md:

Client service:
```
cd client-service
mvn spring-boot:run 
```

Order service:
```
cd order-service
mvn spring-boot:run 
```

Charada service:
```
cd charada
npm install
node server.js
```

## Bootstrap with some data

After starting client and order MS as specified on the README, run the following

```
cd seed
node index.js
```

## Build the frontend

```
cd order-app
npm install
npm start
```

The application should be available under: http://localhost:3000/