// src/components/Search.tsx

import * as React from 'react';
import * as moment from 'moment';

import { IOrderFilter, IOrder } from '../models';

import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';

interface IProps {
  onSearch: (filter: IOrderFilter) => void;
  filter: IOrderFilter,
  orders: IOrder[];
}

class Search extends React.Component<IProps, IOrderFilter> {
  constructor(props: IProps) {
    super(props);
    this.state = props.filter;
  }

  public handleSearch() {
    return (e: any) => {
      console.log('searching with this state:', this.state);
      this.props.onSearch(this.state)
    }
  }

  public handleChange(name: string) {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const val = {...this.state.clientFilter, [name]: e.target.value }
      this.setState({...this.state, ['clientFilter']: val});
    };
  }

  public handleDateChange(name: string) {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const newDate = moment(e.target.value).toDate()
      this.setState({...this.state, [name]: newDate});
    };
  }


  public render() {
    return (
      <Paper className="search">
        <Typography variant="title" id="modal-title">
          Search
        </Typography>

        <div>
          <TextField
            id="start-local"
            label="Start date"
            type="date"
            defaultValue={moment(this.props.filter.startDate).format('YYYY-MM-DD')}
            className='searchControl'
            onChange={this.handleDateChange('startDate')}
            InputLabelProps={{
              shrink: true,
            }}
          />

          <TextField
            id="end-date"
            label="End date"
            type="date"
            defaultValue={moment(this.props.filter.endDate).format('YYYY-MM-DD')}
            onChange={this.handleDateChange('endDate')}
            className='searchControl'
            InputLabelProps={{
              shrink: true,
            }}
          />

          <TextField
            id="name"
            label="Name"
            // value={this.props.}
            className='searchControl'
            onChange={this.handleChange('name')}
            margin="normal"
          />
          <TextField
            id="name"
            label="Phone"
            // value={this.props.}
            className='searchControl'
            onChange={this.handleChange('phone')}
            margin="normal"
          />
          <TextField
            id="name"
            label="E-Mail"
            // value={this.props.}
            className='searchControl'
            onChange={this.handleChange('email')}
            margin="normal"
          />
        </div>

        <Button 
          variant="outlined"
          color="primary"
          className={"aqui"}
          onClick={this.handleSearch()}>
          Search
        </Button>

      </Paper>
      );
  }
}

export default Search;
