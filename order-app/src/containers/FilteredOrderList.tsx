// src/containers/FilteredOrderList.tsx

import { connect } from 'react-redux';

import { IState } from '../state/state';
import OrderList from '../components/OrderList';

function mapStateToProps({ orders }: IState) {
  return {
    orders
  }
}

export const FilteredOrderList =  connect(mapStateToProps)(OrderList);