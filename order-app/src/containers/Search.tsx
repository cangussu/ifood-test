// src/containers/Search.tsx

import { connect } from 'react-redux';

import * as actions from '../state/actions';
import { IState } from '../state/state';

import Search from '../components/Search';
import { IOrderFilter } from '../models';

function mapStateToProps({ filter, orders }: IState) {
  return {
    filter,
    orders
  }
}

function mapDispatchToProps(dispatch: any) {
  return {
    onSearch: (filter: IOrderFilter) => dispatch(actions.fetchOrders(filter)),
  }
}

export const SearchContainer = connect(mapStateToProps,mapDispatchToProps)(Search);