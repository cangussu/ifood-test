import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';

import './index.css';
import registerServiceWorker from './registerServiceWorker';

import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk';
import {orderApp, initialState} from './state/store'


const store = createStore(orderApp, applyMiddleware(thunk))

import * as actions from './state/actions'

store.dispatch(actions.fetchOrders(initialState.filter))

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root') as HTMLElement
);

registerServiceWorker();
