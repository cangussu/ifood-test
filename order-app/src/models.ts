export interface IClient {
  name: string;
  email: string;
  phone: string;
}

export interface IOrder {
  client: IClient;
  restaurantId: string;
  createdAt: Date;
  confirmedAt: Date;
  items: IItem[];
}

export interface IItem {
  description: string;
  quantity: number;
  price: number;
}

export interface IClientFilter {
  name: string;
  email: string;
  phone: string;
}

export interface IOrderFilter {
  startDate: Date;
  endDate: Date;
  clientFilter: IClientFilter;
}
