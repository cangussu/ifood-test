import {IOrderFilter, IOrder} from '../models'

import * as backend from './backend'

// Actions

export const SET_ORDER_FILTER = 'SET_ORDER_FILTER';

export function setFilter(filter: string) {
  return {
    type: SET_ORDER_FILTER,
    filter
  }
}

export const RECEIVE_ORDERS = 'RECEIVE_ORDERS';

export function hackSetOrders(orders: any) {
  return {
    type: RECEIVE_ORDERS,
    orders
  }
}

function deSerializeOrder(data: any) : IOrder {
  return {
    client: {
      name: data.client.name,
      email: data.client.email,
      phone: data.client.phone
    },
    items: data.items ? data.items : [],
    createdAt: data.createdAt ? new Date(data.createdAt) : new Date(),
    confirmedAt: data.confirmedAt ? new Date(data.confirmedAt) : new Date() ,
    restaurantId: data.restaurantId,
  }
}

export function setOrders(data: any) {
  return {
    type: RECEIVE_ORDERS,
    orders: data._embedded.orders.map((order: any) => deSerializeOrder(order)),
  }
}

export const REQUEST_ORDERS = 'REQUEST_ORDERS';

export function requestOrders(filter: IOrderFilter) {
  return {
    type: REQUEST_ORDERS,
    filter
  }
}

//
// Fetcher
//
export function fetchOrders(filter: IOrderFilter) {
  return (dispatch: any) => {
    // First dispatch: the app state is updated to inform
    // that the API call is starting.
​    dispatch(requestOrders(filter))
    return backend.search(filter).then(
      response => dispatch(setOrders(response.data)),
      error => { console.error(error) }
    );
  }
}
