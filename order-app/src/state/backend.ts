import {IOrderFilter} from '../models'
import axios from 'axios'

export function search(filter: IOrderFilter) {
  const params = Object.assign({}, filter, {
    start: filter.startDate.toUTCString(),
    end: filter.endDate.toUTCString()
  });

  return axios.get('/search', {params});
}
