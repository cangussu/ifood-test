import {IOrder, IOrderFilter} from '../models'

export interface IState {
  readonly loading: boolean;
  readonly filter: IOrderFilter;
  readonly orders: IOrder[];
}
