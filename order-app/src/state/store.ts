import {
  REQUEST_ORDERS,
  RECEIVE_ORDERS,
  SET_ORDER_FILTER
} from '../state/actions'​

import {IState} from '../state/state'


export const initialState : IState = {
  loading: false,
  orders: [],
  filter: {
    startDate: new Date("2017-01-02"),
    endDate: new Date(),
    clientFilter: {
      email: '',
      phone: '',
      name: ''
    }
  }
}


export function orderApp(state : IState = initialState, action: any) : IState {
  switch (action.type) {
    case SET_ORDER_FILTER:
      return Object.assign({}, state, {
        filter: action.filter
      })

    case REQUEST_ORDERS:
      return Object.assign({}, state, {
        loading: true
      })

    case RECEIVE_ORDERS:
      return Object.assign({}, state, {
        loading: false,
        orders: action.orders
      })

    default:
      return state
  }
}
