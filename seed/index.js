const axios = require('axios');
const faker = require('faker');

const clientUrl = 'http://localhost:8081/v1/clients/'
const orderUrl = 'http://localhost:8082/v1/orders/'

const totalClients = 10;
const totalOrders = faker.random.number() % 20;
const totalItems = faker.random.number() % 10;

console.log('Total clients: ' + totalClients)
console.log('Orders per client: ' + totalOrders)
console.log('Items per order ' + totalItems)

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function createClient(name, phone, email) {
  return {
    name: faker.name.findName(),
    email: faker.internet.email(),
    phone: faker.phone.phoneNumber('(###) ###-#### x#####')
  }
}

function createItems() {
  return [...Array(totalItems).keys()].map(i => {
    return {
      description: faker.lorem.sentence(),
      quantity: faker.random.number() % 15,
      price: faker.finance.amount(1.99, 200)
    }
  })
}

function createOrder(clientId) {
  return {
    clientId,
    restaurantId: faker.random.uuid(),
    createdAt: faker.date.past(),
    confirmedAt: faker.date.past(),
    items: createItems()
  }
}

for (let i = 0; i < totalClients; i++) { 
  axios.post(clientUrl, createClient())
    .then(response => {
      let client = response.data;
      for (let j = 0; j < totalOrders; j++) { 
        axios.post(orderUrl, createOrder(client.id))
      }
    })
    .catch(error => {
      console.log(error);
    });
}

